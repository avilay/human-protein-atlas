import os.path as path
import sqlite3


def clear_all(conn):
    cur = conn.cursor()
    cur.execute('DELETE FROM proteins')
    cur.execute('DELETE FROM labels')
    conn.commit()

def insert_labels(conn, labelsfile):
    sql = 'INSERT INTO labels (label_id, label) VALUES (?, ?)'
    cur = conn.cursor()
    with open(labelsfile, 'rt') as f:
        for line in f:
            label_id, label = line.split('.')
            label_id = int(label_id)
            label = label.strip()
            cur.execute(sql, [label_id, label])
    conn.commit()

def insert_train(conn, trainfile):
    sql = 'INSERT INTO proteins (protein_id, label_id) VALUES (?, ?)'
    cur = conn.cursor()
    with open(trainfile, 'rt') as f:
        next(f)  # skip the header
        for line in f:
            protein_id, label_ids = line.split(',')
            protein_id = protein_id.strip()
            label_ids = label_ids.split()
            for label_id in label_ids:
                label_id = int(label_id)
                cur.execute(sql, [protein_id, label_id])
    conn.commit()

def main():
    dirname = path.dirname(__file__)
    labelsfile = path.join(dirname, 'labels.txt')
    trainfile = path.join(dirname, 'train.csv')
    conn = sqlite3.connect('train.db')

    clear_all(conn)
    insert_labels(conn, labelsfile)
    insert_train(conn, trainfile)


if __name__ == '__main__':
    main()
