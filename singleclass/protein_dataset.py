import os.path as path
from copy import copy
import torch
from torch.utils.data import Dataset
import imageio
from tqdm import tqdm
from typing import List, Any, Tuple, Dict
from sqlite3 import Connection
import pickle


class IndexedKeys:
    def __init__(self, keys: List[Any]) -> None:
        self._idx_to_key = copy(keys)
        self._key_to_idx = { self._idx_to_key[i]: i for i in range(len(self._idx_to_key)) }  # noqa

    def __getitem__(self, i: int) -> Any:
        return self._idx_to_key[i]

    def __iter__(self) -> Any:
        for key in self._idx_to_key:
            yield key

    def __len__(self) -> int:
        return len(self._idx_to_key)

    def index(self, key: Any) -> int:
        return self._key_to_idx[key]

    def __str__(self) -> str:
        ret = ''
        for i, key in enumerate(self._idx_to_key):
            ret += f'[{i}] = {key}\n'
        return ret


class ProteinDataset(Dataset):
    """
    Creates a 4D image (channel first) from the 4 protein images on disk.

    The actual protein dataset is a multilabel dataset. But here that problem is being modeled as a
    single label problem. This class is parameterized by the label in consideration to set the right
    target variable value. If the protein has the label specified, the target is set as 1, otherwise 0.
    """

    def __init__(self, conn: Connection, label: str, dataroot: str, cache=False) -> None:
        self.dataroot = dataroot
        self._labelnames: IndexedKeys = self._load_labelnames(conn)
        self._protein_guids: IndexedKeys = self._load_protein_guids(conn)
        self._labels: IndexedKeys = self._load_labels(conn, label)
        self._protein_images: Dict[str, torch.Tensor] = self._load_protein_images(dataroot)

    def _load_labelnames(self, conn: Connection) -> IndexedKeys:
        cur = conn.cursor()
        cur.execute('SELECT label_id, label FROM labels')
        rows = cur.fetchall()
        labelnames = [None] * len(rows)
        for row in rows:
            label_id = row[0]
            labelname = row[1]
            labelnames[label_id] = labelname
        cur.close()
        return IndexedKeys(labelnames)

    def _load_protein_guids(self, conn: Connection) -> IndexedKeys:
        cur = conn.cursor()
        cur.execute('SELECT DISTINCT protein_id FROM proteins')
        proteins = []
        for row in cur:
            protein = row[0]
            proteins.append(protein)
        cur.close()
        return IndexedKeys(proteins)

    def _load_labels(self, conn: Connection, label: str) -> IndexedKeys:
        cur = conn.cursor()
        labels = [0] * len(self._protein_guids)
        label_id = self._labelnames.index(label)
        cur.execute('SELECT protein_id FROM proteins WHERE label_id = ?', [label_id])
        rows = cur.fetchall()
        for row in rows:
            protein = row[0]
            protein_idx = self._protein_guids.index(protein)
            labels[protein_idx] = 1
        cur.close()
        return IndexedKeys(labels)

    def _load_protein_images(self, dataroot: str) -> Dict[str, torch.Tensor]:
        protein_images: Dict[str, torch.Tensor] = {}
        for protein_guid in tqdm(self._protein_guids):
            greenfile = path.join(self.dataroot, f'{protein_guid}_green.png')
            green = torch.tensor(imageio.imread(greenfile))

            bluefile = path.join(self.dataroot, f'{protein_guid}_blue.png')
            blue = torch.tensor(imageio.imread(bluefile))

            redfile = path.join(self.dataroot, f'{protein_guid}_red.png')
            red = torch.tensor(imageio.imread(redfile))

            yellowfile = path.join(self.dataroot, f'{protein_guid}_yellow.png')
            yellow = torch.tensor(imageio.imread(yellowfile))

            img = torch.stack([green, blue, red, yellow])
            protein_images[protein_guid] = img
        return protein_images

    def __len__(self) -> int:
        return len(self._protein_guids)

    def __getitem__(self, i: int) -> Tuple[torch.Tensor, torch.Tensor]:
        protein_guid = self._protein_guids[i]
        img = self._protein_images[protein_guid]
        label = self._labels[i]
        return img, label

    def save(self):
        pklfile = path.join(self.dataroot, 'train.pkl')
        with open(pklfile, 'wb') as f:
            pickle.dump(self._protein_images, f, pickle.HIGHEST_PROTOCOL)
