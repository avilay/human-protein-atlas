import os.path as path
import sqlite3
from .protein_dataset import ProteinDataset
from torch.utils.data import DataLoader
from datetime import datetime

PROJ_HOME = '/home/avilay/human-protein-atlas'


def main():
    traindb = path.join(PROJ_HOME, 'local-data', 'train.db')
    conn = sqlite3.connect(traindb)
    print('Creating dataset')
    start = datetime.now()
    pd = ProteinDataset(conn, 'Nucleoplasm', '/data/human-protein-atlas/train')
    end = datetime.now()
    print(f'Dataset created in {end-start}')
    img, lbl = pd[0]
    print('Label: ', lbl)
    print('Saving dataset')
    start = datetime.now()
    pd.save()
    end = datetime.now()
    print(f'Dataset saved in {end-start}')


if __name__ == '__main__':
    main()
