import os.path as path
from collections import defaultdict
import numpy as np
import sqlite3
# from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import f1_score

NUM_CLASSES = 28

def load(conn):
    cur = conn.cursor()

    train = defaultdict(list)
    cur.execute('SELECT protein_id, label_id FROM proteins')
    for row in cur.fetchall():
        protein_id = row[0]
        label_id = row[1]
        train[protein_id].append(label_id)
    proteins = list(train.keys())
    m = len(proteins)
    labels = np.zeros((m, NUM_CLASSES))
    for i, protein in enumerate(proteins):
        lids = train[protein]
        for lid in lids:
            labels[i, lid] = 1.

    return proteins, labels

class Classifier:
    def __init__(self, conn):
        sql = '''
        SELECT label_id, count(label_id) as freq
        FROM proteins
        GROUP BY label_id
        ORDER BY freq
        '''
        cur = conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        self.label_probs = {}
        for row in rows:
            label_id = row[0]
            count = row[1]
            prop = count / 31072
            self.label_probs[label_id] = prop

    def gen_labels(self):
        labels = []
        for label, prob in self.label_probs.items():
            has_label = np.random.choice([True, False], p=[prob, 1-prob])
            if has_label:
                labels.append(label)
        return labels

    def classify(self, m):
        y_pred = np.zeros((m, NUM_CLASSES))
        for i in range(m):
            labels = self.gen_labels()
            for label in labels:
                y_pred[i, label] = 1
        return y_pred


def main():
    traindb = path.join(path.dirname(__file__), 'train.db')
    conn = sqlite3.connect(traindb)

    print('Loading data...')
    proteins, labels = load(conn)

    print('Classifying...')
    m = len(proteins)
    classifier = Classifier(conn)
    pred_labels = classifier.classify(m)
    f1 = f1_score(labels, pred_labels, average='macro')
    print(f1)


if __name__ == '__main__':
    main()
